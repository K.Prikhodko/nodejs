const yargs = require('yargs')
const {addNote, removeNote, getNotes} = require("./notes");

yargs.command({
    command: 'add',
    describe: 'Add note',
    builder: {
        title: {
            describe: 'Note title',
            demandOption: true,
            type: 'string'
        },
        body: {
            describe: 'Note body',
            demandOption: true,
            type: 'string'
        }
    },
    handler({ title, body }) {
        addNote({ title, body })
    }
})

yargs.command({
    command: 'remove',
    describe: 'Remove notes',
    builder: {
        title: {
            describe: 'Note title',
            demandOption: true,
            type: 'string'
        }
    },
    handler({title}) {
        removeNote(title)
    }
})

yargs.command({
    command: 'get',
    describe: 'Get notes',
    handler() {
        getNotes()
    }
})

yargs.parse()
