# NodeJSTestTask

**Description:** The main goal of this project - study of technologies that allow you to create full-scale web applications. 

##Task 1

**Target:** Create script using ECMA5 syntax. Script should use functions: `filter`, `map`, `reduce` and some func. from `Math`.

**Description:** 

1. There are employees and departments.
2. Department may have many employees. It may not be.

**Task should use:**

- Function to calculate the average salary across the department;
- Function for counting - how much money the department spends on salary (reduce);
- Function to get a list of employees with salary below average (filter);
- Function to create an object that includes:
  - the name of the department;
  - the name of the employee;
  - the difference between the employee and the average department;
  
**Stack:** JavaScript ECMA5, NodeJS
