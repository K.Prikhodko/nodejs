const fs = require('fs')

const getNotes = () => {
    console.log(loadNotes())
    return
}

const getNote = ({notes, title}) => {
    return notes.find((note) => note.title === title)
}

const addNote = ({title, body}) => {
    const notes = loadNotes()
    const existingNote = getNote({notes, title})

    if (existingNote) {
        notes.forEach((note, index) => {
            if (note.title === existingNote.title) {
                notes[index] = {title, body}
                console.log('Note ', title, ' has been changed successfully!')
            }
        })
    } else {
        notes.push({title, body})
        console.log('Note ', title, ' was added successfully!')
    }


    saveNotes(notes)
}

const saveNotes = (notes) => {
    fs.writeFileSync('test.json', JSON.stringify(notes))
}

const loadNotes = () => {
    try {
        const file = fs.readFileSync('test.json')
        return JSON.parse(file.toString())
    } catch (e) {
        return []
    }
}

const removeNote = (title) => {
    const notes = loadNotes()
    const existingNote = getNote({notes, title})

    if(existingNote) {
        const updatedNotes = notes.filter(note => note.title !== existingNote.title)
        saveNotes(updatedNotes)
    }
}

module.exports = {
    getNotes,
    addNote,
    removeNote
}
